package it.uniba.plt;

import static org.junit.Assert.assertEquals;

import org.junit.Assert.*;
import org.junit.Test;

import it.uniba.plt.Translator;

public class TranslatorTest {

	@Test
	public void testInputPhrase() {
		String inputPhrase = "hello world";
		Translator translator = new Translator(inputPhrase);
		assertEquals("hello world", translator.getFrase());
		}

	@Test
	public void testTranslationEmptyPhrase() {
		String inputPhrase = "";
		Translator translator = new Translator(inputPhrase);
		assertEquals(Translator.NIL, translator.translate());
		
	}
	
	@Test
	public void testTranslationPhraseStartingWithVowelEndingWithY() {
		String inputPhrase = "any";
		Translator translator = new Translator(inputPhrase);
		assertEquals("anynay", translator.translate());		
	}
	
	@Test
	public void testTranslationPhraseStartingWithVowelEndingWithVowel() {
		String inputPhrase = "apple";
		Translator translator = new Translator(inputPhrase);
		assertEquals("appleyay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithVowelEndingWithConsonant() {
		String inputPhrase = "ask";
		Translator translator = new Translator(inputPhrase);
		assertEquals("askay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithOneConsonant() {
		String inputPhrase = "hello";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithMoreConsonant() {
		String inputPhrase = "known";
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("ownknay", translator.translate());
	}
	
	@Test
	public void testTranslationMoreWords() {
		String inputPhrase = "hello word";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay ordway", translator.translate());
	}
	
	
	@Test
	public void testTranslationCompositeWords() {
		String inputPhrase = "well-being";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellway-eingbay", translator.translate());
	}
	
	@Test
	public void testTranslationWhitExclamationMark() {
		String inputPhrase = "hello word!";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay ordway!", translator.translate());
	}
	
	@Test
	public void testTranslationWhitPoint() {
		String inputPhrase = "hello word.";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay ordway.", translator.translate());
	}
	
	@Test
	public void testTranslationWhitParenthesis() {
		String inputPhrase = "(hello word)";
		Translator translator = new Translator(inputPhrase);
		assertEquals("(ellohay ordway)", translator.translate());
	}
	
	@Test
	public void testTranslationPhrase() {
		String inputPhrase = "hello world, today is first-december! il (primo)";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway, odaytay isay irstfay-ecemberday! ilay (imopray)", translator.translate());
	}
	
	
}
