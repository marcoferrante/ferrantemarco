package it.uniba.plt;

public class Translator {

	private String phrase;
	public static final String NIL = "nil";

	public Translator(String inputPhrase) {
		phrase = inputPhrase;
	}

	public String getFrase() {
		return phrase;
	}

	public String translate() {

		String risultato = "";
		String ris;
		String[] paroleConSpazio;
		String[] paroleConTrattino;
		String frase;
		paroleConSpazio = phrase.split(" ");

		String guardia = "";
		String guardia2 = "";

		for (int i = 0; i < paroleConSpazio.length; i++) {
			guardia = "";
			guardia2 = "";
			if (!phrase.isEmpty() && (paroleConSpazio[i].contains(".") || paroleConSpazio[i].contains(",") ||
					paroleConSpazio[i].contains("!")
					|| paroleConSpazio[i].contains(";") || paroleConSpazio[i].contains(":")
					|| paroleConSpazio[i].contains("?") || paroleConSpazio[i].contains("'")
					|| paroleConSpazio[i].contains("(") || paroleConSpazio[i].contains(")"))) {
				switch (paroleConSpazio[i].substring(paroleConSpazio[i].length() - 1)) {

				case "!":
					guardia = "!";
					paroleConSpazio[i] = paroleConSpazio[i].substring(0, paroleConSpazio[i].length() - 1);
					break;

				case ".":
					guardia = ".";
					paroleConSpazio[i] = paroleConSpazio[i].substring(0, paroleConSpazio[i].length() - 1);
					break;

				case ",":
					guardia = ",";
					paroleConSpazio[i] = paroleConSpazio[i].substring(0, paroleConSpazio[i].length() - 1);
					break;

				case "?":
					guardia = "?";
					paroleConSpazio[i] = paroleConSpazio[i].substring(0, paroleConSpazio[i].length() - 1);
					break;

				case ";":
					guardia = ";";
					paroleConSpazio[i] = paroleConSpazio[i].substring(0, paroleConSpazio[i].length() - 1);
					break;

				case ":":
					guardia = ":";
					paroleConSpazio[i] = paroleConSpazio[i].substring(0, paroleConSpazio[i].length() - 1);
					break;

				case "'":
					guardia = "'";
					paroleConSpazio[i] = paroleConSpazio[i].substring(0, paroleConSpazio[i].length() - 1);
					break;

				case "(":
					guardia = "(";
					paroleConSpazio[i] = paroleConSpazio[i].substring(0, paroleConSpazio[i].length() - 1);
					break;

				case ")":
					guardia = ")";
					paroleConSpazio[i] = paroleConSpazio[i].substring(0, paroleConSpazio[i].length() - 1);
					break;

				default:
					guardia = "";
				}

				switch (paroleConSpazio[i].substring(0, 1)) {

				case "!":
					guardia2 = "!";
					paroleConSpazio[i] = paroleConSpazio[i].substring(1, paroleConSpazio[i].length());
					break;

				case ".":
					guardia2 = ".";
					paroleConSpazio[i] = paroleConSpazio[i].substring(1, paroleConSpazio[i].length());
					break;

				case ",":
					guardia2 = ",";
					paroleConSpazio[i] = paroleConSpazio[i].substring(1, paroleConSpazio[i].length());
					break;

				case "?":
					guardia2 = "?";
					paroleConSpazio[i] = paroleConSpazio[i].substring(1, paroleConSpazio[i].length());
					break;

				case ";":
					guardia2 = ";";
					paroleConSpazio[i] = paroleConSpazio[i].substring(1, paroleConSpazio[i].length());
					break;

				case ":":
					guardia2 = ":";
					paroleConSpazio[i] = paroleConSpazio[i].substring(1, paroleConSpazio[i].length());
					break;

				case "'":
					guardia2 = "'";
					paroleConSpazio[i] = paroleConSpazio[i].substring(1, paroleConSpazio[i].length());
					break;

				case "(":
					guardia2 = "(";
					paroleConSpazio[i] = paroleConSpazio[i].substring(1, paroleConSpazio[i].length());
					break;

				case ")":
					guardia2 = ")";
					paroleConSpazio[i] = paroleConSpazio[i].substring(1, paroleConSpazio[i].length());
					break;

				default:
					guardia2 = "";
				}
			}
			
			if (!(guardia2.isEmpty())) 
				risultato += guardia2;

			if (paroleConSpazio[i].contains("-")) {
				paroleConTrattino = paroleConSpazio[i].split("-");
				for (int n = 0; n < paroleConTrattino.length; n++) {
					
					if (!(guardia2.isEmpty())) 
						risultato += guardia2;
					
					ris = traduci(paroleConTrattino[n]);
					risultato += (ris);

					if (n < paroleConTrattino.length - 1)
						risultato += ("-");
					if (!(guardia.isEmpty()) && n == paroleConTrattino.length - 1)
						risultato += guardia+" ";
				}
			} else {
				ris = traduci(paroleConSpazio[i]);
				risultato += (ris);

				if (!(guardia.isEmpty()))
					risultato += guardia;				
				
				if (i < paroleConSpazio.length - 1)
					risultato += (" ");
			}
			

		}
		return risultato;
	}

	public String traduci(String phrase) {

		String iniziale, resto, risultato = phrase;
		int i = 0;
		if (phrase.isEmpty()) {
			risultato = NIL;
		} else {
			while (!("aeiou".contains(phrase.substring(i, i + 1)))) {
				i++;
			}
			if (i > 0) {
				iniziale = phrase.substring(0, i);
				resto = phrase.substring(i);
				risultato = resto.concat(iniziale).concat("ay");
			} else if (startWithVowel(phrase)) {
				if (phrase.endsWith("y"))
					return phrase.concat("nay");

				if (this.endWithVowel())
					return phrase.concat("yay");

				if (!this.endWithVowel())
					return phrase.concat("ay");
			}
		}
		return risultato;
	}

	public boolean startWithVowel(String phrase) {
		return (phrase.startsWith("a") || phrase.startsWith("e") || phrase.startsWith("i") || phrase.startsWith("o")
				|| phrase.startsWith("u"));
	}

	public boolean endWithVowel() {
		return (phrase.endsWith("a") || phrase.endsWith("e") || phrase.endsWith("i") || phrase.endsWith("o")
				|| phrase.endsWith("u"));
	}

}
